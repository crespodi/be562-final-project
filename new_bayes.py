import pandas as pd, KL, numpy as np

canon = 'norm_firth_rlog1418_combat.csv'
def calc_prop(dataframe):
    """ Creates Control Dataframe and Huntingtons dataframe"""
    df = pd.read_csv(dataframe, index_col=0)
    df = df[~df.index.duplicated(keep='first')]
    # for testing on a subset of the genes
    subset_df = df    
    # create dfs for control and HD
    ctrl_df = subset_df.loc[:, subset_df.columns.str.startswith('C_')]
    HD_df = subset_df.loc[:, subset_df.columns.str.startswith('H_')]
    # calculated Proportion for each dataframe
    ctrl_quant_df = KL.prop_exp(ctrl_df.copy())
    HD_quant_df = KL.prop_exp(HD_df.copy())
    return ctrl_quant_df, HD_quant_df

def calc_canon_quantile(dataframe):
    C_df, H_df = calc_prop(dataframe)
    C_dft = C_df.T
    H_dft = H_df.T
    #calculating the upper and lower quantiles
    C_low_val = C_df.min(axis=1).min()
    C_high_val = C_df.max(axis=1).max()
    H_low_val = H_df.min(axis=1).min()
    H_high_val = H_df.max(axis=1).max()
    
    #Calculate high and low values for the proportions
    val_dict = {'C': [C_low_val, C_high_val], 'H': [H_low_val, H_high_val]}
    H_dft[(H_dft <= H_high_val) & (H_dft >= (H_high_val *.66))] = 100 #high
    H_dft[(H_dft >= H_low_val) & (H_dft <= (H_high_val *.33))] = 10 #low
    H_dft[(H_dft > H_high_val*.33) & (H_dft < (H_high_val*.66))] = 50 #med
    
    H_dft = H_dft.replace(100, value='HIGH')
    H_dft = H_dft.replace(10, value='LOW')
    H_dft = H_dft.replace(50, value='MED')

    C_dft[(C_dft <= C_high_val) & (C_dft >= (C_high_val *.66))] = 100 #high
    C_dft[(C_dft >= C_low_val) & (C_dft <= (C_high_val *.33))] = 10 #low
    C_dft[(C_dft > C_high_val*.33) & (C_dft < (C_high_val*.66))] = 50 #med

    C_dft = C_dft.replace(100, value='HIGH')
    C_dft = C_dft.replace(10, value='LOW')
    C_dft = C_dft.replace(50, value='MED')
    
    
    c_cols = C_dft.columns
    h_cols = H_dft.columns
    LOW_vals = []
    MED_vals = []
    HI_vals = []
    # Count the values of low med and hi for each genes
    for cols in c_cols:
        vals = C_dft[cols].tolist()
        LOW = vals.count('LOW')
        MED = vals.count('MED')
        HI = vals.count('HIGH')
        LOW_vals.append(LOW)
        MED_vals.append(MED)
        HI_vals.append(HI)
# Add columns with low medium and hi values
    C_df['LOW'] = LOW_vals
    C_df['MED'] = MED_vals
    C_df['HIGH'] = HI_vals

    
    H_LOW_vals = []
    H_MED_vals = []
    H_HI_vals = []
    for cols in h_cols:
        vals = H_dft[cols].tolist()
        LOW = vals.count('LOW')
        MED = vals.count('MED')
        HI = vals.count('HIGH')
        H_LOW_vals.append(LOW)
        H_MED_vals.append(MED)
        H_HI_vals.append(HI)
# # Add columns with low medium and hi values
    H_df['LOW'] = H_LOW_vals
    H_df['MED'] = H_MED_vals
    H_df['HIGH'] = H_HI_vals
    smidge = 0.01
    H_final = (H_df[['LOW', 'MED', 'HIGH']] + smidge).div(34)
    C_final = (C_df[['LOW', 'MED', 'HIGH']] + smidge).div(60)

    
    #H_final.to_csv('Hunt_with_smidge.csv', sep=',')
    #C_final.to_csv('control_with_smidge.csv', sep=',')
    return val_dict, H_final, C_final

calc_canon_quantile(canon)
def calc_quant(dataframe):
    prob_df = pd.read_csv(dataframe, index_col=0)
    prob_df = prob_df[~prob_df.index.duplicated(keep='first')]
    # for testing on a subset of the genes
    subset_df = prob_df
    # create dfs for control and HD
    ctrl_df = subset_df.loc[:, subset_df.columns.str.startswith('C_')]
    HD_df = subset_df.loc[:, subset_df.columns.str.startswith('H_')]
    new_df = ctrl_df.merge(HD_df, left_index=True, right_index=True)
    new_df_prob = KL.prop_exp(new_df)
    new_c = new_df_prob.copy()
    new_h = new_df_prob.copy()
    # calculated Proportion for each dataframe
    
    quantiles = calc_canon_quantile(dataframe)[0]
    C_low = quantiles['C'][0]
    C_high = quantiles['C'][1]
    H_low = quantiles['H'][0]
    H_high = quantiles['H'][1]

    new_c[(new_c <= C_high) & (new_c >= (C_high *.66))] = 100 #high
    new_c[(new_c >= C_low) & (new_c <= (C_high *.33))] = 10 #low
    new_c[(new_c > C_high*.33) & (new_c < (C_high*.66))] = 50 #med

    new_c = new_c.replace(100, value='HIGH')
    new_c = new_c.replace(10, value='LOW')
    new_c = new_c.replace(50, value='MED')


    new_h[(new_h <= H_high) & (new_h >= (H_high *.66))] = 100 #high
    new_h[(new_h >= H_low) & (new_h <= (H_high *.33))] = 10 #low
    new_h[(new_h > H_high*.33) & (new_h < (H_high*.66))] = 50 #med

    new_h = new_h.replace(100, value='HIGH')
    new_h = new_h.replace(10, value='LOW')
    new_h = new_h.replace(50, value='MED')

    return new_c, new_h

def calc_class(dataframe):
    P_H = np.divide(5, 100000)
    P_C = 1-P_H
    C_dist = pd.read_csv('control_with_smidge.csv')
    H_dist = pd.read_csv('Hunt_with_smidge.csv')

    c_df, h_df = calc_quant(canon)
    C_cols = c_df.columns
    H_cols = h_df.columns
    Control_probs = []
    HD_probs = []
    for cols in C_cols:
        vals = c_df[cols].tolist()
        for idx, item in enumerate(vals):
            Control_probs.append(C_dist[item].iloc(idx))
        for idx, item in enumerate(vals):
            HD_probs.append(H_dist[item].iloc(idx))
        Class_H =  (np.prod(np.array(HD_probs)))*P_H
        Class_C = (np.prod(np.array(Control_probs)))*P_C
        if Class_H > Class_C:
            print('Huntington', cols)
        else:
            print('Control', cols)
            
            
    

calc_class(canon)    







