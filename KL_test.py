import pandas as pd, numpy as np
from scipy import stats
import random
import sys

# Without C_0018 and H_0014 already
#df = pd.read_csv('norm_firth_rlog1418_combat.csv', sep=',')
#df = df.drop_duplicates("gene_id")

def readfile(dataframe):
    """Reads files"""
    df = pd.read_csv(dataframe)
    return df

def pick_random(data):
    """Randomly pick 3 HD and 6 controls from dataset
        1) returns a dataframe with gene ID + 9 samples
        2) returns a dataframe with the rest of samples (-9 samples)"""
    data = data.drop_duplicates("gene_id")
    # Dataframe sample ID/Control ID/HD ID
    ids = [s for s in list(data) if "mRNASeq" in s]
    control_ids = [ _ for _ in ids if _.startswith('C')]
    HD_ids = [ _ for _ in ids if _.startswith('H')]
    # Randomly pick 3 HD ID and 6 Control ID
    choice_HD = random.sample(HD_ids, 3)
    choice_control = random.sample(control_ids, 6)
    todrop = ['gene_id'] + choice_HD + choice_control
    # Dataframe with gene ID + 3 random HD + 6 random control
    df9 = data[todrop]
    take = list(np.setdiff1d(list(data),todrop))
    take = ['gene_id'] + [s for s in take if "mRNASeq" in s]
    # Rest of samples from data
    rest85 = data[take]
    return df9, rest85

def prop_exp(data):
    """Calculates proportions of each gene: gene count/total counts in that gene.
        In other words, counts proportion of each cell/sum of row
        Returns dataframe with gene proportions for each gene"""
    data = data.copy()
    cols = list(data)
    # divides all rows/sum of rows
    if cols[0] == 'gene_id':
        data.loc[:,cols[1]:cols[-1]] = data.loc[:,cols[1]:cols[-1]].div(data.sum(axis=1), axis=0)
    else:
        data.loc[:,cols[0]:cols[-1]] = data.loc[:,cols[0]:cols[-1]].div(data.sum(axis=1), axis=0)
    return data

def median(data):
    """Finds median of both HD and control for each gene from dataframe
        Returns dataframe with gene_id and median_HD and median_control"""
    ids = [s for s in list(data) if "mRNASeq" in s]
    control_ids = [ _ for _ in ids if _.startswith('C')]
    HD_ids = [ _ for _ in ids if _.startswith('H')]
    data = data.copy()
    data['median_HD'] = data[HD_ids].median(axis=1)
    data['median_control'] = data[control_ids].median(axis=1)
    cols = list(data)
    cols = cols[:1] + cols[-2:]
    med = data[cols]
    return med

def mean(data):
    """Finds mean of both HD and control for each gene from dataframe
        Returns dataframe with gene_id and median_HD and median_control"""
    ids = [s for s in list(data) if "mRNASeq" in s]
    control_ids = [ _ for _ in ids if _.startswith('C')]
    HD_ids = [ _ for _ in ids if _.startswith('H')]
    data = data.copy()
    data['mean_HD'] = data[HD_ids].mean(axis=1)
    data['mean_control'] = data[control_ids].mean(axis=1)
    cols = list(data)
    cols = cols[:1] + cols[-2:]
    mean = data[cols]
    return mean

def prop_sample(data):
    """Finds proportion of genes per sample.
        In other words, counts proportion of each cell/sum of columns
        Returns dataframe with gene proportion for each sample"""
    t = data.T.copy()
    t = t.rename(columns=t.iloc[0]).drop(t.index[0])
    cols = list(t)
    t.loc[:,cols[0]:cols[-1]] = t.loc[:,cols[0]:cols[-1]].div(t.sum(axis=1), axis=0)
    t = t.T
    t = t.reset_index()
    t = t.rename(columns = {'index':'gene_id'})
    return t

def match(q, e):
    """df1 = query dataframe; df2 = exp dataframe
        Purpose is to return a df1 with matching gene_ids to df2"""
    query = q.copy()
    exp = e.copy()
    if list(query)[0] != "gene_id":
        query.columns.values[0] = 'gene_id'
    query['gene_id'] = query['gene_id'].str.split('.').str[0]
    query = query.drop_duplicates("gene_id")
    if list(exp)[0] != "gene_id":
        exp.columns.values[0] = 'gene_id'
    exp['gene_id'] = exp['gene_id'].str.split('.').str[0]
    exp = exp.drop_duplicates("gene_id")
    gene = pd.DataFrame(exp['gene_id'])
    t = pd.merge(gene, query, on='gene_id')
    return t

def kl(p, q):
    """Takes p list and q list, where p is true/observable value and q is to estimate q"""
    kl = stats.entropy(p, qk = q, base=2)
    return kl

def count_class(p, q):
    """p = Observable values, q = estimation (experimental data)"""
    HD = q['mean_HD'].tolist()
    c = q['mean_control'].tolist()
    HD_count = 0
    c_count = 0
    p_cols = list(p)[1:]
    for column in p_cols:
        query_dist = p[column].tolist()
        Class_H = kl(query_dist, HD)
        Class_C = kl(query_dist, c)
        if Class_H < Class_C:
            Class = 'Huntington'
        #print('Class = Huntington', column, Class_H, Class_C)
        else:
            Class = 'Control'
        #print('Class = Control', column, Class_C, Class_H)
        if Class.startswith('H') and column.startswith('H'):
            HD_count += 1
        elif Class.startswith('C') and column.startswith('C'):
            c_count += 1
    #print('True Huntington', HD_count, 'True Control', c_count)
    return HD_count, c_count

def repeat(df, n):
    """repeat count_class function n times to get % True HD, % True control"""
    counter = 0
    count_hd = 0
    count_control = 0
    while (counter != n):
        counter += 1
        if (n <= 500):
            if (counter % 50 == 0):
                print("Calculating", counter, "th iteration")
        if (n <= 2000 and n > 500):
            if (counter % 100 == 0):
                print("Calculating", counter, "th iteration")
        if (n > 2000 and n <= 10000):
            if (counter % 500 == 0):
                print("Calculating", counter, "th iteration")
        if (n > 10000):
            if (counter % 1000 == 0):
                print("Calculating", counter, "th iteration")
        tt = pick_random(df)
        mm = match(tt[0], tt[1])
        p = prop_sample(mm)
        qk = prop_sample(mean(tt[1]))
        counts = count_class(p, qk)
        count_hd += int(counts[0])
        count_control += int(counts[1])
    per_hd = (count_hd/(n*3))*100
    per_control = (count_control/(n*6))*100
    return per_hd, per_control

def count_unknown(df1, df2):
    """df1 = unknown df, df2 = exp df"""
    # Match unknown rows to diff exp gene rows
    unknown = match(df1, df2)
    # Find diff exp gene proportions
    exp = prop_sample(mean(df2))
    count_hd = 0
    count_control = 0
    # Find gene proportions in samples
    p = prop_sample(unknown)
    
    q = match(exp, unknown)
    HD = q['mean_HD'].tolist()
    c = q['mean_control'].tolist()
    
    p_cols = list(p)[1:]
    for column in p_cols:
        query_dist = p[column].tolist()
        Class_H = kl(query_dist, HD)
        Class_C = kl(query_dist, c)
        if Class_H < Class_C:
            count_hd += 1
        else:
            count_control += 1
    return count_hd, count_control, len(p_cols)

def main():
    # parse command-line arguments
    if len(sys.argv) < 4:
        print("you must call program as:  ")
        print("python KL.py <exp data> <unknown file or 0> <n repeat if train, else 0>")
        print("")
        sys.exit(1)

    ex = sys.argv[1]
    unknown = sys.argv[2]
    num = sys.argv[3]
    df = readfile(ex)
    print("")
    if int(num) != 0 :
        print("Calculating %HD and %Control from", num,"iterations", ex, "counts file")
        r = repeat(df, int(num))
        print("% True HD:", r[0], "; % True Control:", r[1])
    if ".csv" in str(unknown):
        print("")
        print("Taking estimation from", ex, "file")
        print("Calculating HD from", unknown, "counts file")
        df2 = readfile(unknown)
        res = count_unknown(df2, df)
        print("From", res[2], "total samples, found:")
        print("Possible HD:", res[0], "; Possible Control:", res[1])

if __name__ == "__main__":
    main()