import pandas as pd, KL, numpy as np, sys

def create_sets(dataframe, times):
    """Create training dataframe and validation"""
    df = pd.read_csv(dataframe)
    training = []
    validate = []
    for time in range(times):
        val_df, train_df = KL.pick_random(df)
        val_df = val_df.set_index('gene_id')
        train_df = train_df.set_index('gene_id')
        training.append(train_df)
        validate.append(val_df)
    return training, validate

def split_df(dataframe, subset=False):
    """creates HD dataframe and Ctrl dataframe"""
    if subset is False:
        c_df = dataframe.loc[:, dataframe.columns.str.startswith('C_')]
        h_df = dataframe.loc[:, dataframe.columns.str.startswith('H_')]
    else:
        subset_df = dataframe.head(subset)
        c_df = subset_df.loc[:, subset_df.columns.str.startswith('C_')]
        h_df = subset_df.loc[:, subset_df.columns.str.startswith('H_')]
        
    return c_df, h_df

def proportion(dataframe):
    """Calc proportions for all the counts in the df"""
    calc_porp_df = KL.prop_exp(dataframe)
    return calc_porp_df


    
def categorize(dataframe):
    """iterate through dataframes and calculates porp for all"""
    df = dataframe
    num_samples = len(df.columns)
    smidge = 0.01
    # puts the gene column on top
    dft = df.T
    columns = dft.columns
    # calculates maximimum and minimum values for each gene across all samples  
    df_low = df.min(axis=1)
    df_high = df.max(axis=1)*.66
    df_med = df.max(axis=1)*.33
    # the categorical dataframe
    cat_df = pd.DataFrame()
    #print(df_low['ENSG00000210196.2']) can index into series
    for cols in columns:
        # so I don't get copy of slice df warnings
        column = dft[cols].copy()
        # set values to numbers because you can't set ints to strings in pandas
        column[(column >= df_high[cols])] = 100 
        column[(column < df_high[cols]) & (column > df_med[cols])] = 50
        column[(column >= df_low[cols]) & (column <= df_med[cols])] = 10

        # set ints to categories
        column = column.replace(10, value='LOW')
        column = column.replace(50, value='MED')
        column = column.replace(100, value='HIGH')
        cat_df[cols] = column
    # empty lists to counts categories for each gene
    LOW = []
    MED = []
    HIGH = []
    for cols in columns:
    # iterate through columns count low, med, hi
        cat_vals = cat_df[cols].tolist()
        LOW.append(cat_vals.count('LOW'))
        MED.append(cat_vals.count('MED'))
        HIGH.append(cat_vals.count('HIGH'))
    # Create 3 columns with values of low med high for each gene
    df['LOW'] = LOW
    df['MED'] = MED
    df['HIGH'] = HIGH
    
    # laplace smoothing add smidge, probabilities divide by num of samples
    Final_df =(df[['LOW', 'MED', 'HIGH']] + smidge).div(num_samples).T
    #Final_df = Final_df.reindex(Final_df.index.rename(['LEVEL']))
    Final_df = Final_df.rename_axis('LEVEL')    
    
    return Final_df

def create_datasets(dataframe, runtime, subset=False):
    #create a list of training and validation sets
    training_sets, validate_sets = create_sets(dataframe, runtime)
    # lists with hd or ctrl training and validation sets
    h_train = []
    c_train = []
    h_valid = []
    c_valid = []
    # getting seperate H and C training sets and calculate proportions
    for i in training_sets:
        c_train_df, h_train_df = split_df(i, subset)
        h_train.append(proportion(h_train_df))
        c_train.append(proportion(c_train_df))
    # get validation sets and calculate porportions
    for v in validate_sets:
        c_valid_df, h_valid_df = split_df(v, subset)
        h_valid.append(proportion(h_valid_df))
        c_valid.append(proportion(c_valid_df))
# merge all the validation control dataframes        
    for idx, tsets in enumerate(c_valid):
        df = categorize(tsets)
        if idx == 0:
            merge_df = df
        else:
            merge_df = merge_df.merge(df, left_index=True, right_index=True)
            
    for idx, tsets in enumerate(h_valid):
        df = categorize(tsets)
        if idx == 0:
            h_merge_df = df
        else:
            h_merge_df = merge_df.merge(df, left_index=True, right_index=True)
    return merge_df, h_merge_df, h_train, c_train


def write_csv(dataframe,runtime, subset=False):
    """writes the training sets for Hd and ctrl to a csv"""
    df = dataframe
    runs = runtime
    subset= subset
    merge, h_merge, h_train, c_train = master(df, runs, subset)
    merge.to_csv('control_validation.csv', sep=',')
    h_merge.to_csv('huntington_validation.csv', sep=',')

    
def median_prob(dataframe):
    """Creates a dictionary of all the median values for a gene using the validation sets"""
    df = pd.read_csv(dataframe, sep=',', index_col=0)
    columns= df.columns
    new_cols = []
    for i in columns:
        split = i.split('.')
        new_cols.append(split[0])
    new_dict = dict(zip(columns,new_cols))

    #rename the columns without unique identifiers
    df = df.rename(columns=new_dict)
    col_set = set(df.columns)
    df_dict = {}

    #get median for all genes and create a dictionary of values
    for cols in col_set:
        key = cols
        medians = df[key].median(axis=1)
        df_dict[key]= df[key].median(axis=1).to_dict()

    return df_dict

def distribution(training_dataset,runtime, query_dataset):
    train = training_dataset
    merge_df, h_merge_df, h_train, c_train = master(train, runtime)
    query_df = pd.read_csv(query_dataset)

    h_distr = median_prob('huntington_validation.csv')
    c_distr = median_prob('control_validation.csv')
    for i in h_train:
        df = i
        columns = df.columns
        for cols in columns:
            sample = df[cols]
            max_val = sample.max()
            min_val = sample.min()
            
            total_sum = sample.sum(axis=1)
            prop_df = sample/total_sum
            if sample
            
            
        
        
    print('hello', h_distr)
    
    
    
def main():
    """parse command line"""
    print(len(sys.argv))
    if len(sys.argv) < 1:
        print("Usage: {0} <dataframe> <train_times> [subset], [train? y/n]".format(sys.argv[0]))
        sys.exit(1)
    if len(sys.argv) == 3:
    # main think to run
        dataframe = sys.argv[1]
        runtime = int(sys.argv[2])
        create_datasets(dataframe, runtime)
    elif len(sys.argv) > 3:
        dataframe = sys.argv[1]
        runtime = int(sys.argv[2])
        sub = int(sys.argv[3])
        train = sys.argv[4]
        if train.lower() == 'y' or 'yes':
            write_csv(dataframe, runtime, sub)
        else:
            create_datasets(dataframe, runtime, sub)
#norm_pfirth_rlog_1418_combat.csv            

if __name__ == "__main__":
    main()
    
