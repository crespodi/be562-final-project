import pandas as pd, numpy as np
import KL
from scipy import stats

def quantile(dataframe):
    df = pd.read_csv(dataframe, index_col=0)
    df = df[~df.index.duplicated(keep='first')]
    # for testing on a subset of the genes
    subset_df = df.head(25)    
    # create dfs for control and HD
    ctrl_df = subset_df.loc[:, subset_df.columns.str.startswith('C_')]
    HD_df = subset_df.loc[:, subset_df.columns.str.startswith('H_')]
    ctrl_quant_df = KL.prop_exp(ctrl_df.copy())
    HD_quant_df = KL.prop_exp(HD_df.copy())

    
    # calculate the quantile of the indexes for HD and Control dataframe
    C_low_quantile = ctrl_quant_df.quantile(q=.25, axis=1)
    C_hi_quantile = ctrl_quant_df.quantile(q=.75, axis=1)
    H_low_quantile = HD_quant_df.quantile(q=.25, axis=1)
    H_hi_quantile = HD_quant_df.quantile(q=.75, axis=1)
    print(C_low_quantile, C_hi_quantile, H_low_quantile, H_hi_quantile)
    return(C_low_quantile, C_hi_quantile, H_low_quantile, H_hi_quantile)

quantile('norm_firth_rlog1418_combat.csv')    

def calculate_quant(dataframe):
    df = pd.read_csv(dataframe)
    
    print(df)

#calculate_quant('all_norm_random.csv')

def diff_genes():
    df = pd.read_csv('Control_HD_Median_distributions.csv', sep=',', index_col=0)
    DE_genes = df.index.tolist()
    return DE_genes

def subset_dataframe(dataframe):
    DE_genes = diff_genes()    
    df = pd.read_csv(dataframe)
    gene_ids = df['gene_id'].tolist()
    new_gene_ids = []
    for i in gene_ids:
        slice=i[0:15]
        new_gene_ids.append(slice)
    df['gene_id'] = new_gene_ids
    df = df.drop_duplicates('gene_id')
    df = df.set_index('gene_id')
    # make sure the indexes are the same
    subset_df = df[df.index.isin(DE_genes)]
    return subset_df


#calculate_quant('all_firth_random.csv')

def read_file(dataframe):
    H_list, C_list, distr = distribution()
    index_list = distr.index
    df = pd.read_csv(dataframe)
    gene_ids = df['gene_id'].tolist()
    new_gene_ids = []
    for i in gene_ids:
        slice=i[0:15]
        new_gene_ids.append(slice)
    df['gene_id'] = new_gene_ids
    df = df.drop_duplicates('gene_id')
    df = df.set_index('gene_id')
    # make sure the indexes are the same
    subset_df = df[df.index.isin(index_list)]
    distr = distr[distr.index.isin(subset_df.index)]
    return subset_df, distr

# creates proportion and adds a smidge to it
def create_dataframe(dataframe):
    df = pd.read_csv(dataframe, index_col=0)
    df = df[~df.index.duplicated(keep='first')]
    # for testing on a subset of the genes
    subset_df = df    
    # create dfs for control and HD
    ctrl_df = subset_df.loc[:, subset_df.columns.str.startswith('C_')]
    HD_df = subset_df.loc[:, subset_df.columns.str.startswith('H_')]
    ctrl_quant_df = KL.prop_exp(ctrl_df.copy())
    HD_quant_df = KL.prop_exp(HD_df.copy())
    
    # calculate the quantile of the indexes for HD and Control dataframe
    C_low_quantile = ctrl_quant_df.quantile(q=.25)
    C_hi_quantile = ctrl_quant_df.quantile(q=.75)
    H_low_quantile = HD_quant_df.quantile(q=.25)
    H_hi_quantile = HD_quant_df.quantile(q=.75)

    # Define what LOW MED and HI values then replace numerics with val
    HD_quant_df[HD_quant_df >= H_hi_quantile] = 100
    HD_quant_df[HD_quant_df <= H_low_quantile] = 10
    HD_quant_df[(HD_quant_df > H_low_quantile) & (HD_quant_df < H_hi_quantile)] = 50
    HD_quant_df = HD_quant_df.replace(100, value='HIGH')
    HD_quant_df = HD_quant_df.replace(10, value='LOW')
    HD_quant_df = HD_quant_df.replace(50, value='MED')

    
    ctrl_quant_df[ctrl_quant_df >= C_hi_quantile] = 100
    ctrl_quant_df[ctrl_quant_df <= C_low_quantile] = 10
    ctrl_quant_df[(ctrl_quant_df > C_low_quantile) & (ctrl_quant_df < C_hi_quantile)] = 50
    ctrl_quant_df = ctrl_quant_df.replace(100, value='HIGH')
    ctrl_quant_df = ctrl_quant_df.replace(10, value='LOW')
    ctrl_quant_df = ctrl_quant_df.replace(50, value='MED')
    #transpose the DFs to the genes are on top
    transpose_c = ctrl_quant_df.T
    transpose_h = HD_quant_df.T
    
    c_cols = transpose_c.columns
    h_cols = transpose_h.columns
    LOW_vals = []
    MED_vals = []
    HI_vals = []
    # Count the values of low med and hi for each genes
    for cols in c_cols:
        vals = transpose_c[cols].tolist()
        LOW = vals.count('LOW')
        MED = vals.count('MED')
        HI = vals.count('HIGH')
        LOW_vals.append(LOW)
        MED_vals.append(MED)
        HI_vals.append(HI)
# Add columns with low medium and hi values
    ctrl_quant_df['LOW'] = LOW_vals
    ctrl_quant_df['MED'] = MED_vals
    ctrl_quant_df['HIGH'] = HI_vals

# Create dataframe with only those values
    ctrl_vals = ctrl_quant_df[['LOW','MED','HIGH']].T

    H_LOW_vals = []
    H_MED_vals = []
    H_HI_vals = []
    for cols in h_cols:
        vals = transpose_h[cols].tolist()
        LOW = vals.count('LOW')
        MED = vals.count('MED')
        HI = vals.count('HIGH')
        H_LOW_vals.append(LOW)
        H_MED_vals.append(MED)
        H_HI_vals.append(HI)
    HD_quant_df['LOW'] = H_LOW_vals
    HD_quant_df['MED'] = H_MED_vals
    HD_quant_df['HIGH'] = H_HI_vals
    HD_vals = HD_quant_df[['LOW','MED','HIGH']].T
    smidge = 0.01
    HD_final = HD_vals.div(34) + smidge

    ctrl_final = ctrl_vals.div(60) + smidge
    ctrl_final.to_csv('ctrl_with_smidge.csv', sep=',')
    HD_final.to_csv('HD_with_smidge.csv', sep=',')
        

    return ctrl_quant_df, HD_quant_df

#create_dataframe('norm_firth_rlog1418_combat.csv')    
#computer_divergence('all_norm_random.csv')
#read_file('all_norm_random.csv')
    
    
