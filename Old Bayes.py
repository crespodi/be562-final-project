import pandas as pd, numpy as np
from scipy import stats


def distribution():
    df = pd.read_csv('Control_HD_Median_distributions.csv', sep=',', index_col=0)
    H_list = df['H_probabilities'].tolist()
    C_list = df['C_probabilities'].tolist()
    Prob_H = np.divide(5, 100000)
    Prob_C = 1-Prob_H
    # Posterior_H = np.prod(np.array(H_list))
    # Posterior_C = np.prod(np.array(C_list))
    return H_list, C_list, df

def read_file(dataframe):
    H_list, C_list, distr = distribution()
    index_list = distr.index
    df = pd.read_csv(dataframe)
    gene_ids = df['gene_id'].tolist()
    new_gene_ids = []
    for i in gene_ids:
        slice=i[0:15]
        new_gene_ids.append(slice)
    df['gene_id'] = new_gene_ids
    df = df.drop_duplicates('gene_id')
    df = df.set_index('gene_id')
    # make sure the indexes are the same
    subset_df = df[df.index.isin(index_list)]
    distr = distr[distr.index.isin(subset_df.index)]
    return subset_df, distr
def computer_divergence(dataframe):
    query_df, canon_df = read_file(dataframe)
    query_cols = query_df.columns
    H_list = canon_df['H_probabilities'].tolist()
    C_list = canon_df['C_probabilities'].tolist()
    Huntington_Count = 0
    Control_Count = 0
    for cols in query_cols:
        query_distr = query_df[cols].tolist()
        Class_H = stats.entropy(query_distr, qk=H_list)
        Class_C = stats.entropy(query_distr, qk=C_list)
        if Class_H < Class_C:
            Class = 'Huntington'
            print('Class = Huntington', cols, Class_H)
        else:
            Class = 'Control'
            print('Class = Control', cols, Class_C)
        if Class.startswith('H') and cols.startswith('H'):
            Huntington_Count+=1
        elif Class.startswith('C') and cols.startswith('C'):
            Control_Count+=1
    print('True Huntington', Huntington_Count, 'True Control', Control_Count)


    
    
computer_divergence('all_norm_random.csv')
#read_file('all_norm_random.csv')
    
    
