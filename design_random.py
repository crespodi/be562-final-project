import pandas as pd
import os
import random

fn = os.path.abspath('../HD_mRNASeq_sample_info.csv')
fl = os.path.abspath('../../samples/all_salmon_quant_rrna.tsv')

# Read salmon counts and drop H_0014_BA9_mRNASeq sample
df = pd.read_csv(fl, sep="\t")
todrop = ['H_0014_BA9_mRNASeq','C_0018_BA9_mRNASeq']
df = df.drop(todrop, axis=1)

# Read sample info file
sample_info = pd.read_csv(fn, comment='#')
# Dataset ID/HD ID/control ID
dataset_ids = sample_info['Dataset.dataset_id'].tolist()
control_ids = [ _ for _ in dataset_ids if _.startswith('C')]
HD_ids = [ _ for _ in dataset_ids if _.startswith('H')]

# Pick 3 random HD and 3 random control
choice_HD = random.sample(HD_ids, 3)
# ['H_0012_BA9_mRNASeq', 'H_0709_BA9_mRNASeq', 'H_0010_BA9_mRNASeq']
choice_control = random.sample(control_ids, 6)
# ['C_0016_BA9_mRNASeq','C_0003_BA9_mRNASeq', 'C_0010_BA9_mRNASeq', 'C_0074_BA9_mRNASeq', 'C_0070_BA9_mRNASeq', 'C_0053_BA9_mRNASeq']

# FIXED (non-changing) to drop list
todrop = todrop + ['H_0012_BA9_mRNASeq', 'H_0709_BA9_mRNASeq', 'H_0010_BA9_mRNASeq'] + ['C_0016_BA9_mRNASeq','C_0003_BA9_mRNASeq','C_0010_BA9_mRNASeq','C_0074_BA9_mRNASeq','C_0070_BA9_mRNASeq','C_0053_BA9_mRNASeq']

# column15: datasetid, 2: subject type, 5: death age
samples = sample_info.iloc[:,[14,1,4]]
# Rename columns
samples.columns = ["Data_id", "Subject_type", "Subject_death"]
# Drop any samples in todrop
samples = samples[~samples['Data_id'].isin(todrop)]

##################### For sample_info design ######################
sample_i = pd.DataFrame(df.columns)
sample_i = sample_i.drop(0)
sample_i = sample_i.rename(columns = {0:'Data_id'})
df_new = pd.merge(sample_i,samples, on='Data_id')
df_new.to_csv("all_info_design_random.csv", index=False)
